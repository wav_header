CFLAGS = -Wall -Wextra -pedantic -pedantic-errors -std=c99 -D_BSD_SOURCE

EXECUTABLE = wav_header

$(EXECUTABLE): $(EXECUTABLE).o

clean:
	rm -f *.o ~* $(EXECUTABLE)
