#!/usr/bin/env python
#
# wav_header - write the header of a wav file
#
# Copyright (C) 2010  Antonio Ospite <ospite@studenti.unina.it>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

import sys
import struct

def hexstring_to_bytes(hex_string):
    res = ""
    for i in range(0, len(hex_string), 2):
            res += chr(int(hex_string[i:i+2], 16))

    return res

def write_wav_header(out_file, fmt, codec_private_data, data_len):

    extradata = hexstring_to_bytes(codec_private_data)

    fmt['cbSize'] = len(extradata)
    fmt_len = 18 + fmt['cbSize']
    wave_len = len("WAVEfmt ") + 4 + fmt_len + len('data') + 4

    out_file.write("RIFF")
    out_file.write(struct.pack('<L', wave_len))
    out_file.write("WAVEfmt ")
    out_file.write(struct.pack('<L', fmt_len))
    out_file.write(struct.pack('<H', fmt['wFormatTag']))
    out_file.write(struct.pack('<H', fmt['nChannels']))
    out_file.write(struct.pack('<L', fmt['nSamplesPerSec']))
    out_file.write(struct.pack('<L', fmt['nAvgBytesPerSec']))
    out_file.write(struct.pack('<H', fmt['nBlockAlign']))
    out_file.write(struct.pack('<H', fmt['wBitsPerSample']))
    out_file.write(struct.pack('<H', fmt['cbSize']))
    out_file.write(extradata)
    out_file.write("data")
    out_file.write(struct.pack('<L', data_len))

if __name__ == "__main__":

    codec_private_data = "008800000f0000000000"

    fmt = {}
    fmt['wFormatTag'] = 0x161
    fmt['nChannels'] = 2
    fmt['nSamplesPerSec'] = 48000
    fmt['nAvgBytesPerSec'] = 192000 / 8
    fmt['wBitsPerSample'] = 16
    fmt['nBlockAlign'] = 8192
    fmt['cbSize'] = 0

    data_len = 70680576

    write_wav_header(sys.stdout, fmt, codec_private_data, data_len)
